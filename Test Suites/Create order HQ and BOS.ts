<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Create order HQ and BOS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-09T10:22:08</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a5f42011-d28f-4210-9c17-6a65383cfe00</testSuiteGuid>
   <testCaseLink>
      <guid>9cbd4a3d-3160-4eb0-b5c2-96a4200426f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Order BOS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f4d5fcb-03b2-47b9-a8cc-04598355f137</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Order HQ</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>241f26d7-01af-4be3-a3f0-893d2f01f5ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login HQ Get Account From Excel</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7d40c85a-5e32-4200-a718-7a4b9d642f7e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>07744282-465e-4dcb-bb81-93d00cb20fa8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b3a4d4ef-2990-493d-87a4-04300910361c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Order BOS Manual</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
