<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Shipper HQ Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-04T11:11:54</lastRun>
   <mailRecipient>n70rohani@gmail.com;ayusinaga67@gmail.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c4ab31c8-a2f4-4860-ba5e-34441184b521</testSuiteGuid>
   <testCaseLink>
      <guid>3caf9fd9-9a37-4386-95b1-d8bd80f2f557</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login HQ Get Account From Excel</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>5799fa05-c862-4baa-9ce7-5925f188bc00</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data File 1/akun shipper</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>5799fa05-c862-4baa-9ce7-5925f188bc00</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>7d40c85a-5e32-4200-a718-7a4b9d642f7e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>5799fa05-c862-4baa-9ce7-5925f188bc00</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>07744282-465e-4dcb-bb81-93d00cb20fa8</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
