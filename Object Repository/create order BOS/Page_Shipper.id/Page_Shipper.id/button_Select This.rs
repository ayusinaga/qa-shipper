<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Select This</name>
   <tag></tag>
   <elementGuidId>148421f4-5323-4cf6-8d3b-8d4938d396ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;regularTBody&quot;)/tr[8]/td[5]/button[@class=&quot;btn btn-sm btn-default&quot;][count(. | //button[(text() = 'Select This' or . = 'Select This')]) = count(//button[(text() = 'Select This' or . = 'Select This')])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@onclick=&quot;setRate(9000,0,4,1,2,1,'JNE','CTC','1 - 2 day(s)','50','4347','0','1', '0.5000', '1', '1')&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-sm btn-default</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>setRate(9000,0,4,1,2,1,'JNE','CTC','1 - 2 day(s)','50','4347','0','1', '0.5000', '1', '1')</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select This</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;regularTBody&quot;)/tr[8]/td[5]/button[@class=&quot;btn btn-sm btn-default&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>//button[@onclick=&quot;setRate(9000,0,4,1,2,1,'JNE','CTC','1 - 2 day(s)','50','4347','0','1', '0.5000', '1', '1')&quot;]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//tbody[@id='regularTBody']/tr[8]/td[5]/button</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='FW : 1 KG'])[8]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='VW : 0.5000 KG -'])[8]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AW : 1 KG -'])[9]/preceding::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//tr[8]/td[5]/button</value>
   </webElementXpaths>
</WebElementEntity>
