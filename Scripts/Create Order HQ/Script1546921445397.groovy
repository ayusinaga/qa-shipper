import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://hq.shipper.id/hqs/login')

WebUI.setText(findTestObject('Object Repository/create order HQ/Page_Shipper HQ Login/input_Shipper_username'), 'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/create order HQ/Page_Shipper HQ Login/input_Shipper_password'), 
    'YRgxeAfm52FOtBp2HCG1sg==')

WebUI.click(findTestObject('Object Repository/create order HQ/Page_Shipper HQ Login/input_Shipper_login'))

WebUI.click(findTestObject('Object Repository/create order HQ/Page_Shipper.id/input_Welcome to Shipper HQ Wh'))

WebUI.click(findTestObject('Object Repository/create order HQ/Page_Shipper.id/a_Fusindo Bekasi'))

WebUI.click(findTestObject('Object Repository/create order HQ/Page_Shipper.id/a_Select Via Map'))

WebUI.click(findTestObject('Object Repository/create order HQ/Page_Shipper.id/area'))

WebUI.click(findTestObject('Object Repository/create order HQ/Page_Shipper.id/button_Use This Address'))

WebUI.setText(findTestObject('Object Repository/create order HQ/Page_Shipper.id/input_Item QTY_itemName'), 'katalog')

WebUI.waitForElementVisible(findTestObject('create order HQ/Page_Shipper.id/button_Get Rates'), 2)

WebUI.click(findTestObject('Object Repository/create order HQ/Page_Shipper.id/button_Get Rates'))

WebUI.waitForElementVisible(findTestObject('create order HQ/Page_Shipper.id/button_Select This'), 2)

WebUI.click(findTestObject('create order HQ/Page_Shipper.id/button_Select This'))

WebUI.waitForElementVisible(findTestObject('create order HQ/Page_Shipper.id/input_Full Name_consignerFulln'), 2)

WebUI.setText(findTestObject('Object Repository/create order HQ/Page_Shipper.id/input_Full Name_consignerFulln'), 'Akbar Jelek')

WebUI.setText(findTestObject('Object Repository/create order HQ/Page_Shipper.id/input_Phone_consignerPhone'), '082380807464356')

WebUI.waitForElementVisible(findTestObject('create order HQ/Page_Shipper.id/input_Phone_consigneePhone'), 2)

WebUI.setText(findTestObject('Object Repository/create order HQ/Page_Shipper.id/input_Full Name_consigneeFulln'), 'AKbar Jelek sekaliiiiii')

WebUI.setText(findTestObject('Object Repository/create order HQ/Page_Shipper.id/input_Phone_consigneePhone'), '0237647351273')

WebUI.click(findTestObject('Object Repository/create order HQ/Page_Shipper.id/span_'))

WebUI.click(findTestObject('Object Repository/create order HQ/Page_Shipper.id/button_Create'))

WebUI.closeBrowser()

