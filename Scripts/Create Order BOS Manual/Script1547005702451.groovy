import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://bos.shipper.id/boss/auth/login')

WebUI.setText(findTestObject('Object Repository/Create Order BOS Manual/Page_BOS PORTAL  Shipper.id/input_BOS PORTAL_identity'), 
    'tio.andiramim@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Create Order BOS Manual/Page_BOS PORTAL  Shipper.id/input_BOS PORTAL_pwd'), 
    'RigbBhfdqOBGNlJIWM1ClA==')

WebUI.click(findTestObject('Object Repository/Create Order BOS Manual/Page_BOS PORTAL  Shipper.id/button_Masuk'))

WebUI.click(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/i_Pesanan Domestik_fa fa-angle'))

WebUI.waitForElementVisible(findTestObject('Create Order BOS Manual/Page_Shipper.id/a_Buat Pesanan Baru'), 2)

WebUI.click(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/a_Buat Pesanan Baru'))

WebUI.click(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/a_COCOWORK Senopati Jl. Tulodo'))

WebUI.click(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/div_Area Tujuan_selectize-inpu'))

WebUI.setText(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/input'), 'Medan')

WebUI.waitForElementVisible(findTestObject('Create Order BOS Manual/Page_Shipper.id/Page_Shipper.id/div_Medan Area Medan'), 
    3)

WebUI.click(findTestObject('Create Order BOS Manual/Page_Shipper.id/Page_Shipper.id/div_Medan Area Medan'))

WebUI.waitForElementVisible(findTestObject('Create Order BOS Manual/Page_Shipper.id/Page_Shipper.id/div_Kota Matsum I'), 
    2)

WebUI.click(findTestObject('Create Order BOS Manual/Page_Shipper.id/Page_Shipper.id/div_Kota Matsum I'))

WebUI.setText(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/input_Item QTY_itemName'), 'Buku')

WebUI.click(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/button_Cek Tarif'))

WebUI.click(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/button_Select This'))

WebUI.setText(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/input_Nama Lengkap_consigneeFu'), 
    'Anny')

WebUI.setText(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/input_Nomor Telepon_consigneeP'), 
    '0817150596')

WebUI.setText(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/textarea_Alamat 1_destinationA'), 
    'jalan saja')

WebUI.click(findTestObject('Object Repository/Create Order BOS Manual/Page_Shipper.id/button_Simpan'))

WebUI.closeBrowser()

