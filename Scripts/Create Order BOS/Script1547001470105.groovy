import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://bos.shipper.id/boss/auth/login')

WebUI.setText(findTestObject('Object Repository/create order BOS/Page_BOS PORTAL  Shipper.id/input_BOS PORTAL_identity'), 
    'tio.andiramim@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/create order BOS/Page_BOS PORTAL  Shipper.id/input_BOS PORTAL_pwd'), 
    'RigbBhfdqOBGNlJIWM1ClA==')

WebUI.click(findTestObject('Object Repository/create order BOS/Page_BOS PORTAL  Shipper.id/button_Masuk'))

WebUI.click(findTestObject('Object Repository/create order BOS/Page_Shipper.id/span_Pesanan Domestik'))

WebUI.waitForElementVisible(findTestObject('create order BOS/Page_Shipper.id/span_Buat Pesanan Baru'), 2)

WebUI.click(findTestObject('Object Repository/create order BOS/Page_Shipper.id/span_Buat Pesanan Baru'))

WebUI.click(findTestObject('Object Repository/create order BOS/Page_Shipper.id/a_COCOWORK Senopati Jl. Tulodo'))

WebUI.click(findTestObject('Object Repository/create order BOS/Page_Shipper.id/span_Lokasi Tujuan (untuk GO-S'))

WebUI.waitForElementVisible(findTestObject('create order BOS/Page_Shipper.id/area'), 2)

WebUI.click(findTestObject('Object Repository/create order BOS/Page_Shipper.id/area'))

WebUI.click(findTestObject('Object Repository/create order BOS/Page_Shipper.id/button_Gunakan Lokasi Ini'))

WebUI.waitForElementVisible(findTestObject('create order BOS/Page_Shipper.id/input_Item QTY_itemName'), 2)

WebUI.setText(findTestObject('Object Repository/create order BOS/Page_Shipper.id/input_Item QTY_itemName'), 'Laptop')

WebUI.waitForElementVisible(findTestObject('create order BOS/Page_Shipper.id/button_Cek Tarif'), 2)

WebUI.click(findTestObject('Object Repository/create order BOS/Page_Shipper.id/button_Cek Tarif'))

WebUI.waitForElementVisible(findTestObject('create order BOS/Page_Shipper.id/Page_Shipper.id/button_Select This'), 2)

WebUI.click(findTestObject('create order BOS/Page_Shipper.id/Page_Shipper.id/button_Select This'))

WebUI.waitForElementVisible(findTestObject('create order BOS/Page_Shipper.id/input_Nama Lengkap_consigneeFu'), 2)

WebUI.setText(findTestObject('Object Repository/create order BOS/Page_Shipper.id/input_Nama Lengkap_consigneeFu'), 'Oni')

WebUI.setText(findTestObject('Object Repository/create order BOS/Page_Shipper.id/input_Nomor Telepon_consigneeP'), '0817150596')

WebUI.setText(findTestObject('create order BOS/Page_Shipper.id/Page_Shipper.id/textarea_Alamat 2_destinationA'), 'Contoh')

WebUI.click(findTestObject('Object Repository/create order BOS/Page_Shipper.id/button_Simpan'))

WebUI.closeBrowser()

